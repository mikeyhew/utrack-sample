# UTrack Sample
### A sample web application made to impress employers. Based on an assignment from the User Interfaces course at the University of Waterloo.

View the live demo at [mikeyhew-utrack-sample.herokuapp.com](https://mikeyhew-utrack-sample.herokuapp.com).

## Getting up and running
1. Clone this repository.
2. Run `npm install` in your terminal from inside the repository.
3. `node app.js` to start the server.
4. Go to localhost:8000 in your browser.
var express = require('express');
var jade = require('jade');
var less = require('less/lib/less-node');
var readFilePromise = require('./fs-promise').readFilePromise;

var templateGlobals = require('./template-globals');

var app = express();

app.set('view engine', 'jade');

function errorMessage(error, res) {
  console.error(error);
  res.status(500).send('HTTP 500 - server error');
}

app.get('/', function (req, res) {
  res.render('app', templateGlobals);
});

var sourceMap;

app.get('styles.map', function (req, res) {
  if (sourceMap) {
    res.send(sourceMap);
  } else {
    res.status(500).send("source map not ready yet");
  }
});

app.get('/styles.css', function (req, res) {
  res.type('css');
  res.set('X-SourceMap', '/styles.map');
  readFilePromise('less/main.less', 'utf-8').then(function (stringOfLess) {
    less.render(stringOfLess, {
      filename: 'less/main.less',
      relativeurls: true,
      developer: true
    }).then(function (output) {
      sourceMap = output.map;
      res.send(output.css);
    }, function (e) {
      errorMessage(e, res);
    });
  }, function (e) {
    errorMessage(e, res);
  });

});

var port = process.env.PORT || 8000;
var ip = process.env.IP;
app.listen(port, ip);
console.log('App listening on ' + (ip || 'localhost') + ':' + port);
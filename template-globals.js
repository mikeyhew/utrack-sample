module.exports = {
  fields: [
    {
      name: 'Activity',
      mixinName: 'select',
      options: ['Swimming', 'Running']
    },
    {
      name: 'Duration (minutes)',
      mixinName: 'number',
    }
  ]
};
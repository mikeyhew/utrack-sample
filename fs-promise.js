(function () {
  var fs = require('fs');

  exports.readFilePromise = function (fileName, options) {
    return new Promise(function (resolve, reject) {
      fs.readFile(fileName, options, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  };
}());